<?php

$routes = array(
    array('home','default','index'),

    array('admin','admin','index'),

    array('listing-abonnes','abonne','index'),
    array('add-abonne','abonne','add'),
    array('single-abonne','abonne','single',array('id')),
    array('edit-abonne','abonne','edit',array('id')),
    array('delete-abonne','abonne','delete',array('id')),

    array('listing-products','product','index'),
    array('add-product','product','add'),
    array('single-product','product','single',array('id')),
    array('edit-product','product','edit',array('id')),
    array('delete-product','product','delete',array('id')),

    array('listing-borrows','borrow','index'),

);









