<?php

namespace App\Controller;

use App\Model\AbonneModel;
use App\Service\Validation;
use App\Service\Form;
use Core\Kernel\AbstractController;

/**
 *
 */
class AbonneController extends AbstractController
{
    private $v;

    public function __construct(){
        $this->v = new Validation();
    }

    public function index(){

        $abonnes = AbonneModel::getAllAbonnesOrderBy();

        $this->render('app.abonne.index',array(
            'abonnes' => $abonnes,
        ),'admin');
    }

    public function single($id) {
        $abonne = $this->getAbonneByIdOr404($id);
        $this->render('app.abonne.single',array(
            'abonne' => $abonne,
        ), 'admin');
    }

    public function add(){
        $errors = array();

        if (!empty($_POST["submitted"])){
            $post = $this->cleanXss($_POST);
            $errors = $this->validate($this->v,$post);
            if($this->v->isValid($errors)) {
                AbonneModel::insert($post);
                $this->addFlash('success', 'Un nouvel abonné a été ajouté !');
                $this->redirect('listing-abonnes');
            }
        }
        $form = new Form($errors);

        $this->render('app.abonne.add',array(
            'form' => $form,
        ), 'admin');
    }

    public function edit($id){
        $abonne = $this->getAbonneByIdOr404($id);
        $errors = array();

        if (!empty($_POST["submitted"])){
            $post = $this->cleanXss($_POST);
            $errors = $this->validate($this->v,$post);
            if($this->v->isValid($errors)) {
                AbonneModel::update($id,$post);
                $this->addFlash('success', 'L\'abonné a bien été modifié');
                $this->redirect('listing-abonnes');
            }
        }
        $form = new Form($errors);

        $this->render('app.abonne.edit',array(
            'form' => $form,
            'abonne' => $abonne,
        ),'admin');
    }

    public function delete($id){
        $abonne = $this->getAbonneByIdOr404($id);
        AbonneModel::delete($id);
        $this->addFlash('success','L\'abonné a bien été supprimé !');
        $this->redirect('listing-abonnes');
    }

    private function getAbonneByIdOr404($id){
        $category = AbonneModel::findById($id);
        if(empty($category)) {
            $this->Abort404();
        }
        return $category;
    }

    private function validate($v,$post)
    {
        $errors = [];
        $errors['nom'] = $v->textValid($post['nom'], 'nom',2, 100);
        $errors['prenom'] = $v->textValid($post['prenom'], 'prenom',2, 100);
        $errors['email'] = $v->emailValid($post['email'],'email');
        return $errors;
    }
}
