<?php

namespace App\Controller;

use Core\Kernel\AbstractController;

/**
 *
 */
class AdminController extends AbstractController
{
    public function index()
    {
        $message = 'Bienvenue sur l\'admin MVC';
        $this->render('app.admin.frontpage',array(
            'message' => $message,
        ),'admin');
    }
}
