<?php

namespace App\Controller;

use App\Model\ProductModel;
use App\Service\Validation;
use App\Service\Form;
use Core\Kernel\AbstractController;

/**
 *
 */
class ProductController extends AbstractController
{
    private $v;

    public function __construct(){
        $this->v = new Validation();
    }

    public function index(){

        $products = ProductModel::getAllProductsOrderBy();

        $this->render('app.product.index',array(
            'products' => $products,
        ),'admin');
    }

    public function single($id) {
        $product = $this->getProductByIdOr404($id);
        $this->render('app.product.single',array(
            'product' => $product,
        ), 'admin');
    }

    public function add(){
        $errors = array();

        if (!empty($_POST["submitted"])){
            $post = $this->cleanXss($_POST);
            $errors = $this->validate($this->v,$post);
            if($this->v->isValid($errors)) {
                ProductModel::insert($post);
                $this->addFlash('success', 'Un nouvel abonné a été ajouté !');
                $this->redirect('listing-products');
            }
        }
        $form = new Form($errors);

        $this->render('app.product.add',array(
            'form' => $form,
        ), 'admin');
    }

    public function edit($id){
        $product = $this->getProductByIdOr404($id);
        $errors = array();

        if (!empty($_POST["submitted"])){
            $post = $this->cleanXss($_POST);
            $errors = $this->validate($this->v,$post);
            if($this->v->isValid($errors)) {
                ProductModel::update($id,$post);
                $this->addFlash('success', 'L\'abonné a bien été modifié');
                $this->redirect('listing-products');
            }
        }
        $form = new Form($errors);

        $this->render('app.product.edit',array(
            'form' => $form,
            'product' => $product,
        ),'admin');
    }

    public function delete($id){
        $product = $this->getProductByIdOr404($id);
        ProductModel::delete($id);
        $this->addFlash('success','L\'abonné a bien été supprimé !');
        $this->redirect('listing-products');
    }

    private function getProductByIdOr404($id){
        $product = ProductModel::findById($id);
        if(empty($product)) {
            $this->Abort404();
        }
        return $product;
    }

    private function validate($v,$post)
    {
        $errors = [];
        $errors['titre'] = $v->textValid($post['titre'], 'titre',2, 100);
        $errors['reference'] = $v->textValid($post['reference'], 'reference',2, 100);
        $errors['description'] = $v->textValid($post['description'], 'description',2, 500);
        return $errors;
    }
}
