<?php

namespace App\Controller;

use App\Model\AbonneModel;
use App\Model\BorrowModel;
use App\Model\ProductModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

/**
 *
 */
class BorrowController extends AbstractController
{
    private $v;

    public function __construct()
    {
        $this->v = new Validation();
    }

    public function index(){
        $errors = array();


        $products = ProductModel::all();
        $abonnes = AbonneModel::all();

        if (!empty($_POST["submitted"])){
            $post = $this->cleanXss($_POST);
            $errors = $this->validate($this->v,$post);
            if($this->v->isValid($errors)) {
                BorrowModel::insert($post);
                $this->addFlash('success', 'Votre nouvelle catégorie a bien été ajoutée');
                $this->redirect('categories');
            }
        }
        $form = new Form($errors);

        $this->render('app.borrow.index',array(
            'products' => $products,
            'abonnes' => $abonnes,
            'form' => $form,
        ),'admin');
    }


    private function validate($v,$post)
    {
        $errors = [];
        $verifAbonne = AbonneModel::findById($post['select-abonne']);
        if (empty($verifAbonne)){
            $errors['select-abonne'] = 'Cet abonné n\'existe pas';
        }
        $verifProduct = ProductModel::findById($post['select-product']);
        if (empty($verifProduct)){
            $errors['select-product'] = 'Ce produit n\'existe pas';
        }
        return $errors;
    }
}
