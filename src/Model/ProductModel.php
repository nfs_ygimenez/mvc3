<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class ProductModel extends AbstractModel{

    protected static $table = 'products';

    public static function getAllProductsOrderBy($column = 'titre',$order = 'ASC'){
        return App::getDatabase()->query("SELECT * FROM ".self::$table. " ORDER BY $column $order" ,get_called_class());
    }

    public static function insert($post){
        App::getDatabase()->prepareInsert("INSERT INTO " . self::$table . " (titre,reference,description) VALUES (?,?,?)",array($post['titre'],$post['reference'],$post['description']));
    }

    public static function update($id,$post){
        App::getDatabase()->prepareInsert("UPDATE " . self::$table . " SET titre = ?, reference = ?, description = ? WHERE id = ?",
            array($post['titre'], $post['reference'],$post['description'], $id)
        );
    }
}