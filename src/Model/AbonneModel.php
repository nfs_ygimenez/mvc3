<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class AbonneModel extends AbstractModel{

    protected static $table = 'abonnes';

    public static function getAllAbonnesOrderBy($column = 'nom',$order = 'ASC'){
        return App::getDatabase()->query("SELECT * FROM ".self::$table. " ORDER BY $column $order" ,get_called_class());
    }

    public static function insert($post){
        App::getDatabase()->prepareInsert("INSERT INTO " . self::$table . " (nom,prenom,email,age,created_at) VALUES (?,?,?,?,NOW())",array($post['nom'],$post['prenom'],$post['email'],$post['age']));
    }

    public static function update($id,$post){
        App::getDatabase()->prepareInsert("UPDATE " . self::$table . " SET nom = ?, prenom = ?, email = ?, age = ? WHERE id = ?",
            array($post['nom'], $post['prenom'],$post['email'],$post['age'], $id)
        );
    }
}