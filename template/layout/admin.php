<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Framework Pédagogique MVC6</title>
    <?php echo $view->add_webpack_style('admin'); ?>
</head>
<body>


<header id="masthead">
    <nav>
        <ul>
            <a href="<?= $view->path(''); ?>">
                <div class="logo">
                <img src="<?php echo $view->asset('img/logo.png'); ?>" alt="">
            </div></a>
            <li><a href="<?= $view->path('admin'); ?>">Statistiques</a></li>
            <li><a href="<?= $view->path('listing-products'); ?>">Produits</a></li>
            <li><a href="<?= $view->path('listing-abonnes'); ?>">Abonnés</a></li>
            <li><a href="<?= $view->path('listing-borrows'); ?>">Emprunts</a></li>
        </ul>
    </nav>
</header>

<div class="body">
    <?php foreach ($view->getFlash() as $flash) {
        echo '<p class="'.$flash['type'].'">'.$flash['message'].'</p>';
    }?>
    <div class="container">
        <?= $content; ?>
    </div>

    <footer id="colophon">
        <div class="wrap">
            <p>MVC 6 - Framework Pédagogique.</p>
        </div>
    </footer>
</div>
<?php echo $view->add_webpack_script('admin'); ?>
</body>
</html>
