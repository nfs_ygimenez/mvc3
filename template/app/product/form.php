<form action="" method="post" novalidate>
    <?php echo $form->label('titre','Titre du produit');?>
    <?php echo $form->input('titre','text', $product->titre ?? '');?>
    <?php echo $form->error('titreA');?>

    <?php echo $form->label('reference','Référence du titre');?>
    <?php echo $form->input('reference','text', $product->reference ?? '');?>
    <?php echo $form->error('reference');?>

    <?php echo $form->label('description','Description du produit');?>
    <?php echo $form->textarea('description', $product->description ?? '');?>
    <?php echo $form->error('description');?>

    <?php echo $form->submit('submitted', $textButton);?>
</form>