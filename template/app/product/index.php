<section id="products">
    <div class="wrap">
        <div class="container">
            <?php foreach ($products as $product){?>
                <div class="single-abonne">
                    <h2><?php echo $product->titre;?></h2>
                    <div class="buttons">
                        <a href="<?php echo $view->path('single-product',array('id' => $product->id));?>">Voir</a>
                        <a href="<?php echo $view->path('edit-product',array('id' => $product->id));?>">Modifier</a>
                        <a href="<?php echo $view->path('delete-product',array('id' => $product->id));?>">Supprimer</a>
                    </div>
                </div>
            <?php }?>
            <a href="<?= $view->path('add-product'); ?>">Ajouter un produit</a>
        </div>
    </div>
</section>