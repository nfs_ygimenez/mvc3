<section id="single-product">
    <div class="wrap">
        <div class="infos">
            <h2><?php echo $product->titre; ?></h2>
            <p>Référence : <?php echo $product->reference; ?></p>
            <p>Description : <?php echo $product->description; ?></p>
        </div>
    </div>
</section>