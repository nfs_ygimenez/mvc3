<form action="" method="post" novalidate>
    <?php echo $form->label('Choisissez un abonné'); ?>
    <?php echo $form->selectEntity('select-abonne', $abonnes, 'nom' , $abonnes->id ?? ''); ?>
    <?php echo $form->error('select-abonne'); ?>

    <?php echo $form->label('Choisissez un produit'); ?>
    <?php echo $form->selectEntity('select-product', $products, 'titre' , $products->id ?? ''); ?>
    <?php echo $form->error('select-product'); ?>

    <?php echo $form->submit('submitted', $textButton);?>
</form>