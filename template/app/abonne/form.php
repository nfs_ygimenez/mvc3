<form action="" method="post" novalidate>
    <?php echo $form->label('nom','Nom de l\'abonné');?>
    <?php echo $form->input('nom','text', $abonne->nom ?? '');?>
    <?php echo $form->error('nom');?>

    <?php echo $form->label('prenom','Prénom de l\'abonné');?>
    <?php echo $form->input('prenom','text', $abonne->prenom ?? '');?>
    <?php echo $form->error('prenom');?>

    <?php echo $form->label('email','Email de l\'abonné');?>
    <?php echo $form->input('email','email', $abonne->email ?? '');?>
    <?php echo $form->error('email');?>

    <?php echo $form->label('age','Âge de l\'abonné');?>
    <?php echo $form->input('age','number', $abonne->age ?? '');?>
    <?php echo $form->error('age');?>

    <?php echo $form->submit('submitted', $textButton);?>
</form>