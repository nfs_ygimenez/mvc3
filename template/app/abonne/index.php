<section id="abonnes">
    <div class="wrap">
        <div class="listing-abonnes">
            <?php foreach ($abonnes as $abonne){?>
                <div class="single-abonne">
                    <h2><?php echo $abonne->nom . ' ' . $abonne->prenom;?></h2>
                    <div class="buttons">
                        <a href="<?php echo $view->path('single-abonne',array('id' => $abonne->id));?>">Voir</a>
                        <a href="<?php echo $view->path('edit-abonne',array('id' => $abonne->id));?>">Modifier</a>
                        <a href="<?php echo $view->path('delete-abonne',array('id' => $abonne->id));?>">Supprimer</a>
                    </div>
                </div>
            <?php }?>
            <a href="<?= $view->path('add-abonne'); ?>">Ajouter un abonné</a>
        </div>
    </div>
</section>