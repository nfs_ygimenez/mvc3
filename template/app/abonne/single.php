<section id="single-abonne">
    <div class="wrap">
        <div class="infos">
            <h2><?php echo $abonne->nom . ' ' . $abonne->prenom; ?></h2>
            <p>Email : <?php echo $abonne->email; ?></p>
            <?php if ($abonne->age != 0){ ?>
                <p>Âge : <?php echo $abonne->age; ?> ans</p>
            <?php } else { ?>
                <p>Âge non renseigné</p>
           <?php } ?>
        </div>
    </div>
</section>